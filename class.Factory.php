<?php

class Factory
{
	// метод для формирования роботов типа MyHydra1, где $i - трубемое количество
	public function createMyHydra1($i) {
		$robots = [];
		while ($i<>0) {
			$robots[] = new MyHydra1();
			$i--;
		}

		return $robots;
	}

	// метод для формирования роботов типа MyHydra1, где $i - трубемое количество
	public function createMyHydra2($i) {
		$robots = [];
		while ($i<>0) {
			$robots[] = new MyHydra2();
			$i--;
		}

		return $robots;
	}

	public function getSpeed($robots) {

		// подсчитываем минимальную скорость среди всех роботов

		$minSpeed = 0;
		$firstLap = true;
		foreach ($robots as $robot) {

			if(!$firstLap) {
				if($robot->speed < $minSpeed) $minSpeed = $robot->speed;
			}
			else {
				// берем скорость первого попавшегося робота, чтобы было с чем сравнивать
				$minSpeed = $robot->speed;
				$firstLap = false;
			}

		}

		return $minSpeed;

	}

	public function getWeight($robots) {

		// подсчитываем минимальную скорость среди всех роботов

		foreach ($robots as $robot) {
			$weight += $robot->weight;
		}

		return $weight;

	}
	
}